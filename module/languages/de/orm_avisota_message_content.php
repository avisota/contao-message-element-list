<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:09+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['listItems']['0'] = 'Listenelemente';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listItems']['1'] = 'Falls Javascript ausgeschaltet ist sichern Sie Ihre Änderungen, bevor Sie die Reihenfolge der Elemente verändern.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listType']['0']  = 'Listentyp';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listType']['1']  = 'Bitte wählen Sie den Typ der Liste aus.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['list_legend']    = 'Listenelemente';
