<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message-element-list
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listType']  = array(
    'List type',
    'Please choose the type of list.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listItems'] = array(
    'List items',
    'If JavaScript is disabled, make sure to save your changes before modifying the order.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['list_legend'] = 'List items';
