<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:48+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['listItems']['0'] = 'Elements da la glista';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listItems']['1'] = 'Sche JavaScript è deactivà stos ti memorisar tias midadas avant che midar la successiun.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listType']['0']  = 'Tip da glista';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['listType']['1']  = 'Tscherna il tip da la glista.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['list_legend']    = 'Elements da la glista';
